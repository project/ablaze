<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<body>

<!-- header starts here -->
	<div id="header">
	<div id="header-content">
		<h1 id="logo"><?php print $site_name;?></h1> 
    <h2 id="slogan">Powered by Drupal
		<?php if ($site_slogan): ?>
		<?php print $site_slogan; ?>
		<?php endif; ?>
		</h2>
		<div class="searchform">
		 <?php if(isset($search_box)) { print($search_box); } ?>  
		 </div>
<?php if ($primary_links): ?> <div id="menu"> <?php print theme('links', $primary_links); ?> </div> <?php endif; ?>
		</div>
</div>
		
	<!-- content-wrap starts here -->
	<div id="content-wrap"><div id="content">	 
	
		<div id="sidebar" >				
			<?php print $right; ?>
      <?php print $left; ?>	
				  
<?php if ($secondary_links): ?>
  <div id="secondary">
    <?php print theme('links', $secondary_links); ?>
  </div> <!-- /#secondary -->
<?php endif; ?>
	
</div>		
    
<?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>

<div id="main">
<?php print $breadcrumb; ?>
<?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
<?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
<?php if ($show_messages): print $messages; endif; ?>
<?php print $help; ?>

<?php print $content; ?>
</div>
 </div></div>   
										
		
	<!-- footer starts here -->	
	<div id="footer"><div id="footer-content">
	
	<?php if ($footer_message || $footer) : ?>
<div id="footer_message">
    <?php print $footer_message . $footer;?>
</div>
<?php endif; ?>
<div id="footer_left">
<?php print $footer_left;?>
</div>
<div id="footer_rigth'">
<?php print $footer_right;?>
</div>
	
		
	
	</div></div>
	<!-- footer ends here -->
<?php print $closure; ?>
</body>
</html>
